import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.sql.Connection
import java.sql.SQLException
import kotlin.io.path.Path
import kotlin.io.path.readLines

class Alumno() {

    val userHistory = mutableListOf<MutableList<String>>()
    var addedHistory = false

    /**
     * Funcion que permite al usuario realizar los ejercicios y poder retomarlos en el punto que se quedó la anterior vez que realizo ejercicios
     */
    fun seguirItinerari():MutableList<MutableList<String>>{
        var menuOption:Int
        var continuarItinerario = true

        for (actuallProblem in problems.problemaList.indices){
            if (!problems.problemaList[actuallProblem].resolted && continuarItinerario) {
                problems.problemaList[actuallProblem].printProblem(actuallProblem)
                println("Vols resoldre'l?")
                println("     1. Si     2. No")
                do {
                    println("Escull una opcio del menu")
                    menuOption = scaner.nextInt()

                    when (menuOption) {
                        1 -> {
                            println()
                            val problemHistory = problems.problemaList[actuallProblem].resolProblem()
                            userHistory.add(problemHistory)
                        }

                        2 -> {
                            println()
                            continuarItinerario = false
                            break
                        }

                        else -> {
                            println("$menuOption No es una opcion del menu")
                            menuOption = 3
                        }
                    }
                } while (menuOption == 3)
            }
            else if (!continuarItinerario){
                break
            }

        }
        return userHistory
    }

    /**
     * Muestra por pantalla todos los problemas y da elegir 1 al usuario para poder resolverlo
     */
    fun listOfProblems(){
        var menuOption:Int
        var nummberOfProblems = 0
        val problemsToSelect = mutableListOf<Problema>()
        println("Selecciona una categoria")
        println("1. Tipues de dades")
        println("2. Condicionals")
        println("3. Bucles")
        println("4. Llistes")
        do {
            println("Escull una opcio del menu")
            menuOption = scaner.nextInt()

            when (menuOption) {
                1 -> {
                    repeat(2){ println() }
                    for (problema in problems.problemaList){
                        if (problema.tema == "Tipus de dades"){
                            println(" - ${problema.titulo}")
                            nummberOfProblems++
                            problemsToSelect.add(problema)
                        }
                    }
                }

                2 -> {
                    repeat(2){ println() }
                    for (problema in problems.problemaList){
                        if (problema.tema == "Condicionals") {
                            println(" - ${problema.titulo}")
                            nummberOfProblems++
                            problemsToSelect.add(problema)
                        }
                    }
                }

                3 -> {
                    repeat(2){ println() }
                    for (problema in problems.problemaList){
                        if (problema.tema == "Bucles") {
                            println(" - ${problema.titulo}")
                            nummberOfProblems++
                            problemsToSelect.add(problema)
                        }
                    }
                }

                4 -> {
                    repeat(2){ println() }
                    for (problema in problems.problemaList){
                        if (problema.tema == "Llistes") {
                            println(" - ${problema.titulo}")
                            nummberOfProblems++
                            problemsToSelect.add(problema)
                        }
                    }
                }

                else -> {
                    println("$menuOption No es una opcion del menu")
                    menuOption = 5
                }
            }
        } while (menuOption == 5)
        do {
            println("Que problema quieres resolver?")
            menuOption = scaner.nextInt()
            if (menuOption in 1..nummberOfProblems){
                problemsToSelect[menuOption-1].printProblem(menuOption-1)
                val problemHistory = problemsToSelect[menuOption-1].resolProblem()
                userHistory.add(problemHistory)
                break
            }
            else{
                println("Problema no encontrado")
                menuOption = nummberOfProblems+1
            }
        }while (menuOption > nummberOfProblems)
    }

    /**
     * Permite al usuario poder ver su historial de errores en los problemas realizados
     */
    fun checkHistory(connection: Connection){
        if (addedHistory){
            val listOfHistory = getHistory(connection)
            val history = listOfHistory.lastIndex as History

            for (i in history.history.indices){
                println()
                println("${blue}${box} ${"Poblema numero ${i + 1}"} ${reset}")
                for (j in history.history[i]){
                    println("   - $j")
                }
            }
        }
        else {
            println("No se ha añadido ningun nuevo hisotrial")
        }
    }

    /**
     * Muesta por pantalla un texto te explica al usuario las funciones del programa
     */
    fun showHelp(){
        repeat(2) { println() }
        println("Jutge_ITB es una programa utilizado para ayudar con el aprendizade de la progrmaacion")
        println()
        println("El progrma cuenta con dos tipos diferentes de usuarios, alumno y profesor, cada uno con diferentes funciones")
        println("Un alumno podra resolver los diferentes problemas con los que cuenta el programa y poder ver su historial de" +
                "intentos en cada problema para asi poder aprender de sus errores")
        println("En cambio, el profesor podra crear nuevos problemas para poder ser resuelto por los alumnos, ademas el profesor" +
                "podra evaluar a los alumnos y ponerles una nota")
    }

    /**
     * Añade el historial del usuario al fichero json
     */
    fun addHistoryDatabase(userHistory:MutableList<MutableList<String>>, connection: Connection){
        val history = History(userHistory)
        val historyToJson = Json.encodeToString(history)

        val json = org.postgresql.util.PGobject()
        json.type = "jsonb"
        json.value = historyToJson

        try {
           connection.prepareStatement("INSERT INTO history (history) VALUES (?)").use { stmt ->
                stmt.setObject(1, json)
                stmt.executeUpdate()
            }
        }catch (e:SQLException){
            println("No se ha podido guardar el historial($e)")
        }

    }

    fun getHistory(connection: Connection):MutableList<History>{

        val listOfHistory :MutableList<History> = mutableListOf()

        val statement = connection.createStatement()
        val resultSet = statement.executeQuery("SELECT history FROM history")

        while (resultSet.next()){
            val history = resultSet.getObject("history") as History
            listOfHistory.add(history)
        }
        return listOfHistory
    }
}


