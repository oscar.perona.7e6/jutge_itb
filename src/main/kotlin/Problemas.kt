import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.sql.Connection
import java.sql.SQLException
import kotlin.io.path.Path
import kotlin.io.path.readLines

class Problemas {
    val problemaList = mutableListOf<Problema>()

    /**
     * Al inicializar la clase lee el fichero json y añade los porblemas a una lista
     */
    fun getProblems(connection: Connection){
        try {
            val query = connection.createStatement()
            val sqlQuery = "SELECT * FROM problema" //Query que se ejecuta en la base de datos
            val resultSet = query.executeQuery(sqlQuery) // Ejecuta la sentencia sql

            while (resultSet.next()) {
                val tema = resultSet.getString("tema")
                val titulo = resultSet.getString("titulo")
                val enunciado = resultSet.getString("enunciado")
                val imputType = resultSet.getString("input_type")
                val outputType = resultSet.getString("output_type")
                val publicImput = resultSet.getString("public_input")
                val publicOutput = resultSet.getString("public_output")
                val privateImput = resultSet.getString("private_input")
                val privateOutput = resultSet.getString("private_output")
                val problema = Problema(tema, titulo, enunciado, imputType, outputType, publicImput, publicOutput, privateImput, privateOutput)
                problemaList.add(problema)
            }
            resultSet.close()

            println("Problema añadido correctamente")
        } catch (e: SQLException) {
            println("Error SQL: ${e.message}")
        }
    }

}