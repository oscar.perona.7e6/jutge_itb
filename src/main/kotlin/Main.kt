/**
 * @author Oscar Perona Gomez
 */

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.Scanner
import kotlin.system.exitProcess

val problems= Problemas()
val scaner = Scanner(System.`in`)

//Colores

val red = "\u001B[53;31m"
val blue = "\u001B[1;34m"
val green = "\u001B[1;32m"
val reset = "\u001B[0m"
val box = "\u001B[51m"

/**
 * Funcion que muestra el inicio del programa y una vez pasados 5 segundos iniciara el sistema de problemas
 */
fun main(){

    val jdbcUrl = "jdbc:postgresql://localhost:5432/jutgeitb"

    // get the connection
    val connection = DriverManager.getConnection(jdbcUrl)

    if (connection.isValid(0)){
        problems.getProblems(connection)
        println("\n" +
                "──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────\n" +
                "─────────${red}██████${reset}─${red}██████${reset}──${red}██████${reset}─${red}██████████████${reset}─${red}██████████████${reset}─${red}██████████████${reset}────${blue}██████████${reset}─${blue}██████████████${reset}─${blue}██████████████${reset}───\n" +
                "─────────${red}██${reset}░░${red}██${reset}─${red}██${reset}░░${red}██${reset}──${red}██${reset}░░${red}██${reset}─${red}██${reset}░░░░░░░░░░${red}██${reset}─${red}██${reset}░░░░░░░░░░${red}██${reset}─${red}██${reset}░░░░░░░░░░${red}██${reset}────${blue}██${reset}░░░░░░${blue}██${reset}─${blue}██${reset}░░░░░░░░░░${blue}██${reset}─${blue}██${reset}░░░░░░░░░░${blue}██${reset}───\n" +
                "─────────${red}██${reset}░░${red}██${reset}─${red}██${reset}░░${red}██${reset}──${red}██${reset}░░${red}██─${reset}${red}██████${reset}░░${red}██████${reset}─${red}██${reset}░░${red}██████████${reset}─${red}██${reset}░░${red}██████████${reset}────${blue}████${reset}░░${blue}████${reset}─${blue}██████${reset}░░${blue}██████${reset}─${blue}██${reset}░░${blue}██████${reset}░░${blue}██${reset}───\n" +
                "─────────${red}██${reset}░░${red}██${reset}─${red}██${reset}░░${red}██${reset}──${red}██${reset}░░${red}██${reset}─────${red}██${reset}░░${red}██${reset}─────${red}██${reset}░░${red}██${reset}─────────${red}██${reset}░░${red}██${reset}──────────────${blue}██${reset}░░${blue}██${reset}───────${blue}██${reset}░░${blue}██${reset}─────${blue}██${reset}░░${blue}██${reset}──${blue}██${reset}░░${blue}██${reset}───\n" +
                "─────────${red}▓▓${reset}░░${red}▓▓${reset}─${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}─────────${red}▓▓${reset}░░${red}▓▓▓▓▓▓▓▓▓▓${reset}──────${blue}▓▓${reset}░░${blue}▓▓${reset}───────${blue}▓▓${reset}░░${blue}▓▓${reset}─────${blue}▓▓${reset}░░${blue}▓▓▓▓▓▓${reset}░░${blue}▓▓▓▓${reset}─\n" +
                "─────────${red}▓▓${reset}░░${red}▓▓${reset}─${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓▓▓▓▓${reset}─${red}▓▓${reset}░░░░░░░░░░${red}▓▓${reset}──────${blue}▓▓${reset}░░${blue}▓▓${reset}───────${blue}▓▓${reset}░░${blue}▓▓${reset}─────${blue}▓▓${reset}░░░░░░░░░░░░${blue}▓▓${reset}─\n" +
                "─${red}▓▓▓▓▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓─${red}▓▓${reset}░░${red}▓▓▓▓▓▓▓▓▓▓${reset}──────${blue}▓▓${reset}░░${blue}▓▓${reset}───────${blue}▓▓${reset}░░${blue}▓▓${reset}─────${blue}▓▓${reset}░░${blue}▓▓▓▓▓▓▓▓${reset}░░${blue}▓▓${reset}─\n" +
                "─${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}─────${red}▓▓${reset}░░${red}▓▓${reset}──${red}▓▓${reset}░░${red}▓▓${reset}─${red}▓▓${reset}░░${red}▓▓${reset}──────────────${blue}▓▓${reset}░░${blue}▓▓${reset}───────${blue}▓▓${reset}░░${blue}▓▓${reset}─────${blue}▓▓${reset}░░${blue}▓▓${reset}────${blue}▓▓${reset}░░${blue}▓▓${reset}─\n" +
                "─${red}▒▒${reset}░░${red}▒▒▒▒▒▒${reset}░░${red}▒▒${reset}─${red}▒▒${reset}░░${red}▒▒▒▒▒▒${reset}░░${red}▒▒${reset}─────${red}▒▒${reset}░░${red}▒▒${reset}─────${red}▒▒${reset}░░${red}▒▒▒▒▒▒${reset}░░${red}▒▒${reset}─${red}▒▒${reset}░░${red}▒▒▒▒▒▒▒▒▒▒${reset}────${blue}▒▒▒▒${reset}░░${blue}▒▒▒▒${reset}─────${blue}▒▒${reset}░░${blue}▒▒${reset}─────${blue}▒▒${reset}░░${blue}▒▒▒▒▒▒▒▒${reset}░░${blue}▒▒${reset}─\n" +
                "─${red}▒▒${reset}░░░░░░░░░░${red}▒▒${reset}─${red}▒▒${reset}░░░░░░░░░░${red}▒▒${reset}─────${red}▒▒${reset}░░${red}▒▒${reset}─────${red}▒▒${reset}░░░░░░░░░░${red}▒▒${reset}─${red}▒▒${reset}░░░░░░░░░░${red}▒▒${reset}────${blue}▒▒${reset}░░░░░░${blue}▒▒${reset}─────${blue}▒▒${reset}░░${blue}▒▒${reset}─────${blue}▒▒${reset}░░░░░░░░░░░░${blue}▒▒${reset}─\n" +
                "─${red}▒▒▒▒▒▒▒▒▒▒▒▒▒▒${reset}─${red}▒▒▒▒▒▒▒▒▒▒▒▒▒▒${reset}─────${red}▒▒▒▒▒▒${reset}─────${red}▒▒▒▒▒▒▒▒▒▒▒▒▒▒${reset}─${red}▒▒▒▒▒▒▒▒▒▒▒▒▒▒${reset}────${blue}▒▒▒▒▒▒▒▒▒▒${reset}─────${blue}▒▒▒▒▒▒${reset}─────${blue}▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒${reset}─\n" +
                "──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────")

        println()
        println("En este programa realizaras ejercicios de programacion, indroduciras el resultado del problema en la consola para que asi")
        println("el programa pueda corregirte para asi saber si esta correcto o no")
        repeat(2){ println() }
        println("PREPARATE QUE COMENZAMOS")
        Thread.sleep(5*1000L)
        logIn(connection)
    }
    else println("ha ocurrido un error al conectarse a la base de datos, saliendo del programa...")

}

/**
 * Pide al usuario que indique su tipo de rol y pide la contraseña en caso de ser necesario
 */
fun logIn(connection:Connection){
    var menuOption = 0
    while (menuOption!=1 && menuOption!=2){
        println("Queres entrar al progrma como alumno o como profesor?")
        println("1. Alumno")
        println("2. Profesor")
        menuOption = scaner.nextInt()

        when (menuOption){
            1 -> {
                val user = Alumno()
                alumnMenu(user, connection)
            }
            2 -> {
                repeat(2){ println() }
                println("Por favor introduce la contraseña para profesores")
                var loged = false
                var passwrdTries = 1
                while(!loged){
                    val passwd = scaner.next()
                    if (passwd == "profesor123"){
                        loged = true
                        println("Contraseña correcta, bienvenido profesor")
                        val user = Profesor()
                        teacherMenu(user, connection)
                    }
                    else if (passwrdTries < 3){
                        passwrdTries++
                        println("Contraseña incorrecta, por favor intentalo de nuevo")
                    }
                    else{
                        println("Limite de intentos alcanzado, redireccionando a la eleccion de rol...")
                        logIn(connection)
                        break

                    }
                }
            }
            else -> println("$menuOption no es una opcion")
        }
    }
}

/**
 * Muestra al usuario con rol alumno el menu de opciones que puede realizar y le permite seleccionar una
 */
fun alumnMenu(user:Alumno, connection: Connection){
    repeat(2){ println() }
    print("        ")
    println("BIENVENIDO ALUMNO")
    println()
    println("1. Seguir con el itinerario de aprendizaje")
    println("2. Lista de problemas")
    println("3. Ver historial de problemas resueltos")
    println("4. Ayuda")
    println("5. Salir")
    println()
    println("Escribe un opcion del menu")
    var menuOption = 0
    while (menuOption!=1 && menuOption!=2 && menuOption!=3 && menuOption!=4 && menuOption!=5){
        menuOption = scaner.nextInt()

        when (menuOption){
            1 -> {
                user.addHistoryDatabase(user.seguirItinerari(), connection)
                user.addedHistory = true
                alumnMenu(user,connection)
            }
            2 -> {
                user.listOfProblems()
                alumnMenu(user,connection)
            }
            3 -> {
                user.checkHistory(connection)
                alumnMenu(user,connection)
            }
            4 -> {
                user.showHelp()
                alumnMenu(user,connection)
            }
            5 -> {
                salir(connection)
            }
            else -> {
                println("$menuOption no es una opcion del menu")
            }
        }
    }
}

/**
 * Muesta al usuario con rol profesor el menu de opciones que puede realizar y le permite seleccionar una
 */
fun teacherMenu(user:Profesor, connection: Connection){
    repeat(2){ println() }
    print("        ")
    println("BIENVENIDO PROFESOR")
    println()
    println("1. Añadir problema")
    println("2. Correguir alumno")
    println("3. Salir")
    println()
    println("Escribe un opcion del menu")
    var menuOption = 0
    while (menuOption!=1 && menuOption!=2){
        menuOption = scaner.nextInt()

        when (menuOption){
            1 -> {
                user.addProblem(connection)
                teacherMenu(user, connection)
            }
            2 -> {
                user.correctUserProblems(connection)
                teacherMenu(user,connection)
            }
            3 -> {
                salir(connection)
            }
            else -> {
                println("$menuOption no es una opcion del menu")
            }
        }
    }
}

fun salir(connection: Connection){
    connection.close()
    println()
    println("Saliendo del programa...")
    exitProcess(0)
}