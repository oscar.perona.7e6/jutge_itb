import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.sql.Connection
import java.sql.SQLException
import kotlin.io.path.Path
import kotlin.io.path.readLines

class Profesor() {

    /**
     * Pregunta al usuario los datos del nuevo problema y una vez introducidos todos añade el problema al fichero json
     */
    fun addProblem(connection: Connection){
        val partOfProblem = listOf("tema","titulo","enunciado","imputType","outputType","publicImput","publicOutput","privateImput","privateOutput")
        val valuesOfProblem = mutableListOf<String>()
        for(problemPart in partOfProblem){
            print("Introduce el $problemPart del nuevo problema: ")
            val inputUsuari = scaner.next() + scaner.nextLine()
            valuesOfProblem.add(inputUsuari)
        }
        val problem = Problema(valuesOfProblem[0],valuesOfProblem[1],valuesOfProblem[2],valuesOfProblem[3],valuesOfProblem[4],valuesOfProblem[5],valuesOfProblem[6],valuesOfProblem[7],valuesOfProblem[8])

        try {
            val statement = connection.prepareStatement("INSERT INTO problema (tema, titulo, enunciado, input_type, output_type, public_input, public_output, private_input, private_output) VALUES (?,?,?,?,?,?,?,?,?)")
            statement.setString(1, problem.tema)
            statement.setString(2, problem.titulo)
            statement.setString(3, problem.enunciado)
            statement.setString(4, problem.imputType)
            statement.setString(5, problem.outputType)
            statement.setString(6, problem.publicImput)
            statement.setString(7, problem.publicOutput)
            statement.setString(8, problem.privateImput)
            statement.setString(9, problem.privateOutput)
            statement.executeUpdate()
            statement.close()
        }catch (e: SQLException){
            println("Ha ocurrido un error al introducir el nuevo problema a la base de datos")
        }

    }

    /**
     * Lee el fichero json que almacena los hisotriales y permite al profesor seleccionar uno para poder correguirlo
     */
    fun correctUserProblems(connection: Connection){

        val listOfHistory = getHistory(connection)

        while (true){
            println("Hay ${listOfHistory.size} alumnos por correguir, indoduce el alumno al que le quieras poner nota(1..${listOfHistory.size})")
            val correctHistoy = scaner.nextInt()

            if (correctHistoy-1 in listOfHistory.indices){
                getMark(listOfHistory[correctHistoy-1])
                break
            }
            else {
                println("Ese alumno no esta en el historial")
                println()
            }
        }
    }

    /**
     * Calcula la nota del historial seleccionado anteriormente
     */
    fun getMark(history:History){

        var mark:Double = (history.history.size * 10 / problems.problemaList.size).toDouble()

        for (i in history.history.indices){
            val incorrectTries = history.history[i].size-history.history.size
            mark -= (incorrectTries * 0.1)
        }

        if (mark < 0) mark = 0.0

        if (mark >= 5){
            println()
            println("$green$box FELICIDADES HAS APROBADO $reset")
            repeat(2){ println() }
            println("Tu nota final es:")
            println()
            println("       $green$box$mark$reset")
            repeat(2){ println() }
        }
        else{
            println()
            println("$red$box LO SIENTO, HAS SUSPENDIDO $reset")
            repeat(2){ println() }
            println("Tu nota final es:")
            println()
            println("       $red$box$mark$reset")
            repeat(2){ println() }
        }
    }

    fun getHistory(connection: Connection):MutableList<History>{

        val listOfHistory :MutableList<History> = mutableListOf()

        try {
            val statement = connection.createStatement()
            val resultSet = statement.executeQuery("SELECT history FROM history")

            while (resultSet.next()){
                val historyContent = resultSet.getString("history")
                val history = Json.decodeFromString<History>(historyContent)
                listOfHistory.add(history)
            }
        }catch (e:SQLException){
            println("Ha ocurrido un error al obtener los historiales")
        }

        return listOfHistory
    }
}