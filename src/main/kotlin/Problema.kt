import kotlinx.serialization.Serializable

/**
 * Clase utilizada para crear problemas, esta contiene todos los datos del problema y las funciones necesarias para trabajar con ellos
 */
@Serializable
class Problema(
    val tema : String,
    val titulo : String,
    val enunciado: String,
    val imputType: String,
    val outputType: String,
    val publicImput: String,
    val publicOutput: String,
    val privateImput: String,
    val privateOutput: String
) {
    var resolted = false
    var tries = 0
    var user_tries:MutableList<String> = mutableListOf()

    /**
     * Funcion que se encarga de mostrar por pantalla todos los datos del problema necesarios(Enunciado,imput,output...)
     */
    fun printProblem(problemNumber:Int){
        repeat(2){ println() }
        println("Problema numero: $problemNumber")
        println()
        println("${blue}${box} ${titulo.uppercase()} ${reset}")
        repeat(2){ println() }
        println(enunciado)
        println(imputType)
        println(outputType)
        println()
        println("Exemple:")
        println("Imput: $publicImput")
        println("Output: $publicOutput")
        println()
        println("Tindras que resoldre el problema amb aquest imput: $privateImput")
    }

    /**
     * Funcion que guarda la respuesta el usuario y comprueba si la respuesta es correcta o no
     */
    fun resolProblem():MutableList<String>{
        do{
            tries++
            println("Escriu el output de la teva solucio:   ")
            val userOutput = readln()
            if(userOutput == privateOutput) {
                user_tries.add(userOutput)
                println()
                println("El output es correscte, segueix aixi!")
                resolted = true
                problemStatus()
            }
            else {
                println("El output en incorrecte")
                user_tries.add(userOutput)
                println("Quantitat d'intents: $tries")
                println("Proba un altre cop")
            }
        }while (!resolted)
        return user_tries
    }

    /**
     * Muestra por pantalla el estado el problema una vez haya sido completado(numero de intentos, lista de intentons y si ha sido completado o no)
     */
    fun problemStatus(){
        println()
        println("Quantitat d'intents: $tries")
        println()
        println("Els teus intents: ")
        for(i in user_tries) println("   - $i")
        println()
        print("Estat: ")
        if(resolted) println("Resolt ")
        else println("No resolt")
        println()
    }

}