import kotlinx.serialization.Serializable
import java.sql.Connection

@Serializable
class History(val history: MutableList<MutableList<String>>) {
}