# JUTGE ITB

## Definicion del proyecto
En este proyecto podras poner a prueba tu nivel de programacion, para ello el programa te mostrara un problema por la consola, junto a este se motraran diferentes juegos de pruebas que constan el imput y el output, estos principalmente son un ejemplo de solucion del problema, junto a ellos estara el imput que tu tendras que utilizar para averiguar el resultado del problema.
Tendras que hacer un programa capaz de resolver el ejercicio y introducir por consola el output que te da el program que has hecho. Si el ejercicio esta resuelto correctamente saltara al sigueinte, en caso de no acertar el resultado tendras que seguir intentando resolver el ejercicio hasta completarlo correctamente   

## Base de datos
Esta version del JutgeITB utiliza una base de datos para la persistencia de datos, gracias a esta el programa puede almacenar y leer todos los problemas y historiales que necesita el Jutge,
La base de datos cuenta con dos tablas:
 - La tabla problema la cual almacena todos los datos de los problemas(enunciado, tipo de problema, imput,output...) en formato string.
 - La tabla history la cual almacena en formato JsonB todos los intentos de todos los problemas que ha realizado el usuario 

## Sistema de datos
El projecto consta de una clase la cual es utilizada para hacer los problemas, cada problema consta de su enunciado, imput y outputs tanto publicos como privados, un contador de intentos y una lista de todos los intentos. Aparte de esto tambien contiene 3 funciones que se utilizan para trabajar con los problemas, estas funciones son:
 - printProblem: Esta funcion se encarga de mostrar por pantalla el enunciado y los juegos de pruebas del problema
 - resolProblem: Se encarga de recoger la respuesta del usuario y comprobar si es correcta o no, segun si lo es o no pasara al siguiente problema o permitira al usuario volverlo a intentar
 - problemStatus: Muestra por pantalla el estado el problema una vez haya sido completado(numero de intentos, lista de intentons y si ha sido completado o no)



## Como ejecutar el proyecto

Para ejecutar el proyecto primero de todo se tiene que descargar el proyecto, para eso dirigite a la terminal y ejecuta el sigueinte comando

`git clone https://gitlab.com/oscar.perona.7e6/jutge_itb.git`

Lo siguiente que tendras que hacer es instalar la base de datos, para ello dirigete el servidor postgres sql de tu ordenador, una vez dentro tendas que ejecutar todos los comandos que se encuentan en el fichero `src/main/dades/SriptsBD.txt` en el terminal de postgres.

Una vez hayas instalado la base de datos en tu servidor postgres lo unico que quedara sera iniciar el programa, para ello dirigete al fihcer `src/main/kotlin/Main.kt` e inicia el programa pulsando el boton iniciar situado en la parte superior derecha de la pantalla

![Voton ejecutar](ejecutarImg.png)
